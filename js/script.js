$( document ).ready(function(){
    $('.news-slider').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:3
            }
        }
     });
    $('.news-update .customNextBtn').click(function() {
        $('.news-slider').trigger('next.owl.carousel');
      });
    $('.news-update .customPreviousBtn').click(function() {
        $('.news-slider').trigger('prev.owl.carousel');
    });

    $('.game-slider').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:false,
        items:1
     });
    
    $('.game-play .customNextBtn').click(function() {
        $('.game-slider').trigger('next.owl.carousel');
      });
    $('.game-play .customPreviousBtn').click(function() {
        $('.game-slider').trigger('prev.owl.carousel');
    });

    $('.heroes-slider').owlCarousel({
        loop:false,
        margin:20,
        nav:false,
        dots:false,
        mouseDrag: false,
        touchDrag: false,
        responsive:{
            0:{
                items:5
            }
            
        }
     });

    $('.game-heroes .owl-item .item').click(function() {
        var raceName =  $(this).attr('data-race-name');
        console.log(raceName);
        $('.game-heroes .owl-item .item').removeClass('hightlight');
        $(this).addClass("hightlight");
        $('.illustration-img img').removeClass("active");
        $('.illustration-img').find("[data-race-name='" + raceName + "']").addClass("active");

        $('.heroes-summary').removeClass("active");
        $(".game-heroes .right-content > [data-race-name='" + raceName + "']").addClass("active");
        
      });

    $('.game-heroes .customNextBtn').click(function() {
        $('.heroes-slider').trigger('next.owl.carousel');
        $('.game-heroes .owl-item .item.hightlight').addClass('old');
        if($('.game-heroes .owl-item .item.hightlight').parent().next().length){
            $('.game-heroes .owl-item .item.hightlight').parent().next().find(".item").addClass('hightlight');
        }else{
            $('.game-heroes .owl-item').first().find(".item").addClass('hightlight');
        }
        
        $('.game-heroes .owl-item .hightlight.old').removeClass('hightlight old');
        var raceName = $('.game-heroes .owl-item .hightlight.item').attr("data-race-name"); 
        $('.illustration-img img').removeClass("active");
        $('.illustration-img').find("[data-race-name='" + raceName + "']").addClass("active");

        $('.heroes-summary').removeClass("active");
        $(".game-heroes .right-content > [data-race-name='" + raceName + "']").addClass("active");
        
      });
    $('.game-heroes .customPreviousBtn').click(function() {
        $('.heroes-slider').trigger('prev.owl.carousel');
        $('.game-heroes .owl-item .item.hightlight').addClass('old');
        
        if($('.game-heroes .owl-item .item.hightlight').parent().prev().length){
           $('.game-heroes .owl-item .item.hightlight').parent().prev().find(".item").addClass('hightlight');
        }else{
            $('.game-heroes .owl-item').last().find(".item").addClass('hightlight');
        }
        $('.game-heroes .owl-item .hightlight.old').removeClass('hightlight old');
        var raceName = $('.game-heroes .owl-item .hightlight.item').attr("data-race-name"); 
        $('.illustration-img img').removeClass("active");
        $('.illustration-img').find("[data-race-name='" + raceName + "']").addClass("active");

        $('.heroes-summary').removeClass("active");
        $(".game-heroes .right-content > [data-race-name='" + raceName + "']").addClass("active");
    });


})

$( document ).ready(function(){
  $("a[href='#scroll-survival']").on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
  $(".js-modal-btn").modalVideo();
});